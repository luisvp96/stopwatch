
import './App.css';
import Countdown from './Components/CountDown/Countdown';
import DigitalClock from './Components/DigitalClock/DigitalClock';
import Stopwatch from './Components/Stopwatch/Stopwatch';

function App() {
  return (
    <div className="App">
        <DigitalClock/>
        <Countdown/>
        <Stopwatch/>
    </div>
  );
}

export default App;
